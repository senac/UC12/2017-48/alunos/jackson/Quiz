<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="../css/bootstrap.min.css" >

        <title>Pesquisa de usuario</title>
    </head>
    <body>

        <div class="container">
            <fieldset>
                <legend>Pesquisa de Usu�rios</legend>
                
                <form class="form-inline" action="/action_page.php">
                    <div class="form-group ">
                        <label class="control-label col-sm-2" for="id" >ID:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="id"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="nome">Nome:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nome" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="apelido">Apelido:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="apelido" >
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Pesquisar</button>
                        </div>
                    </div>
                </form> 
            </fieldset>
        </div>




        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="../js/jquery-3.2.1.slim.min.js" ></script>
        <script src="../js/popper.min.js" ></script>
        <script src="../js/bootstrap.min.js" ></script>
    </body>
</html>