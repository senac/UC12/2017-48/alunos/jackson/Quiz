package br.com.senac.quiz.servlet;

import br.com.senac.quiz.dao.UsuarioDAO;
import br.com.senac.quiz.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UsuarioServlet extends HttpServlet {

    //<%! %>
    private Usuario usuario;
    private UsuarioDAO dao;

    @Override
    protected void doPost(
            HttpServletRequest requisicao,
            HttpServletResponse resposta) throws ServletException, IOException {

        //dasasda
        /*
        <%
           sdfsdfdd     
        %>

        */
      //  PrintWriter saida = resposta.getWriter();

        try {
            String nome = requisicao.getParameter("nome");
            String senha = requisicao.getParameter("senha");
            String apelido = requisicao.getParameter("apelido");

            usuario = new Usuario();
            usuario.setNome(nome);
            usuario.setSenha(senha);
            usuario.setApelido(apelido);

            dao = new UsuarioDAO();
            dao.inserir(usuario);
            
            
            
            RequestDispatcher view =
                    requisicao.getRequestDispatcher("result.jsp");
            
            requisicao.setAttribute("usuario", usuario);
            
            view.forward(requisicao, resposta);

            
            /*
            saida.println("<html>");
            saida.println("<body>");
            saida.println("Salvo com sucesso!");
            saida.println("</body>");
            saida.println("</html>");
*/

        } catch (Exception ex) {
            
            /*
            saida.println("<html>");
            saida.println("<body>");
            saida.println("Falha ao salvar.<br />");
            saida.println(ex);
            saida.println("</body>");
            saida.println("</html>");
            resposta.sendRedirect("/salvo.html");
*/
        }

    }

}
