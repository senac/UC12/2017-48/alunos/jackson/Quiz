/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.quiz.servlet;

import br.com.senac.quiz.dao.UsuarioDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ExclusaoUsuarioServlet", urlPatterns = {"/excluir"})
public class ExclusaoUsuarioServlet extends HttpServlet {

    private UsuarioDAO dao;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter saida = response.getWriter();
        try {

            String param = request.getParameter("id");

            int id = Integer.parseInt(param);
            dao = new UsuarioDAO();
            dao.delete(id);
            saida.print(this.gerarHtml("Excluido com sucesso."));

        } catch (NumberFormatException ex) {
            saida.print(
                    this.gerarHtml("Erro conversao.<br />"
                            + ex.getClass()));

        } catch (Exception ex) {
            saida.print(
                    this.gerarHtml("Erro ao excluir.<br />"
                            + ex.getClass()));
        }
    }

    private String gerarHtml(String mensagem) {
        return String.format(""
                + "<!DOCTYPE html>"
                + "<html>"
                + "<head>"
                + "</head>"
                + "<body>"
                + "Mensagem:%s</body>"
                + "</html>", mensagem);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
