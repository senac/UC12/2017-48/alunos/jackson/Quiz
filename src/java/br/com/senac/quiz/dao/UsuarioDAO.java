package br.com.senac.quiz.dao;

import br.com.senac.quiz.model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UsuarioDAO implements DAO<Usuario> {

    @Override
    public List<Usuario> getLista() {

        List<Usuario> lista = new ArrayList<>();
        Connection connection = Conexao.getConnection();

        try {
            String query = "select * from usuario ;";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                int id = rs.getInt("id");
                String nome = rs.getString("nome");
                String senha = rs.getString("senha");
                String apelido = rs.getString("apelido");

                Usuario usuario = new Usuario(id, nome, senha, apelido);
                lista.add(usuario);
            }
        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("erro eo desconectar");
            }
        }

        return lista;

    }

    public List<Usuario> getListaPorNome(
            int id, String nome, String apelido) {
        List<Usuario> lista = new ArrayList<>();
        Connection connection = Conexao.getConnection();
        try {
            String query = "select * from usuario where 1 = 1 ";

            int param = 0;

            if (id > 0) {
                query += " and id = ? ";
                param++;
            }

            if (nome != null && !nome.isEmpty()) {
                query += " and nome like ? ";
                param++;
            }

            if (apelido != null && !apelido.isEmpty()) {
                query += " and apelido like ? ";
                param++;
            }

            PreparedStatement ps
                    = connection.prepareStatement(query);

            for (int i = 1; i <= param; i++) {

                if (id > 0) {
                    ps.setInt(i, id);
                    i++;
                }

                if (nome != null && !nome.isEmpty()) {
                    ps.setString(i, nome + "%");
                    i++;
                }

                if (apelido != null && !apelido.isEmpty()) {
                    ps.setString(i, apelido + "%");
                    i++;

                }
            }

            System.out.println(ps);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Usuario usuario
                        = new Usuario(
                                rs.getInt("id"),
                                rs.getString("nome"),
                                rs.getString("senha"),
                                rs.getString("apelido"));
                lista.add(usuario);
            }

        } catch (SQLException ex) {
            System.out.println("Erro ...");
            
        }finally{
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Erro ao fechar conexo...");
            }
        }

        return lista;
    }

    public List<Usuario> getListaPorNome(String nome) {
        List<Usuario> lista = new ArrayList<>();
        Connection connection = Conexao.getConnection();

        try {
            String query = "select * from usuario where nome like ? ;";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, nome + "%");
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Usuario usuario
                        = new Usuario(
                                rs.getInt("id"),
                                rs.getString("nome"),
                                rs.getString("senha"),
                                rs.getString("apelido"));
                lista.add(usuario);
            }
        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("erro eo desconectar");
            }
        }

        return lista;
    }

    @Override
    public void inserir(Usuario usuario) {

        //abrir 
        Connection connection = Conexao.getConnection();
        try {
            //definir uma query
            String query = "INSERT INTO usuario (nome , senha , apelido) "
                    + "values (? , ? , ?) ;";
            //executar a query 
            PreparedStatement ps = connection.prepareStatement(query , PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, usuario.getNome());
            ps.setString(2, usuario.getSenha());
            ps.setString(3, usuario.getApelido());

            ps.executeUpdate();
            
            ResultSet rs =  ps.getGeneratedKeys() ;
            rs.first();
            usuario.setId(rs.getInt(1));
            

        } catch (Exception ex) {
            System.out.println("Error ao salvar ... ");
        } finally {
            try {
                //fechar a conexao
                connection.close();
            } catch (Exception ex) {
                System.out.println("Error ao fechar conexao ... ");
            }
        }

    }

    @Override
    public void atualizar(Usuario usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {

        Connection connection = Conexao.getConnection();

        try {
            String query = "Delete from usuario where id= ?;";

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);

            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println("Erro ao  executar operação...");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Erro ao  executar operação...");
            }
        }

    }

    @Override
    public Usuario getPorId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
